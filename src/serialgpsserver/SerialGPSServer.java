/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serialgpsserver;

import java.io.IOException;
import java.util.concurrent.Semaphore;

/**
 *
 * @author Matias
 */
public class SerialGPSServer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
  
        int portNumber;
        portNumber = 7777;
    
        int numberOfPermits = 1;
       // int numberOfProducers = 1;  Fra eksempel, brukes ikke her(enda)
        boolean fairness = true; //Java semaphore
           String comport = "";  
        //Nødvendige Semaphorer
        Semaphore semaphoreGPScoordinates = new Semaphore(numberOfPermits, fairness);
        Semaphore semaphoreWaypoint = new Semaphore(numberOfPermits, fairness);
        Semaphore semaphoreJSONdata = new Semaphore(numberOfPermits,fairness);
        Semaphore semaphorePlatformMode = new Semaphore(numberOfPermits,fairness);
        Semaphore semaphoreNorthEast = new Semaphore(numberOfPermits,fairness);
        Semaphore semaphoreManualData = new Semaphore(numberOfPermits,fairness);
        Semaphore semaphoreArduinoComm = new Semaphore(numberOfPermits, fairness); //"numberOfPermits" må kanskje endres for denne semaforen.
        //Nødvendige fellesobjekt(storagebox)
        StorageBoxGPS storageBoxGPS = new StorageBoxGPS();
        StorageBoxSend storageBoxSend = new StorageBoxSend();
        StorageBoxReceive storageBoxReceive = new StorageBoxReceive();
        StorageBoxPlatformMode storageBoxPlatformMode = new StorageBoxPlatformMode();
        StorageBoxNorthEastGPS storageBoxNorthEast = new StorageBoxNorthEastGPS();
        StorageBoxManualmode storageBoxManualMode = new StorageBoxManualmode();
        StorageBoxArduinoComm storageBoxArduinoComm = new StorageBoxArduinoComm();
        //Oppretter "TrådObjekter"
        ArduinoHandler arduinoHandler = new ArduinoHandler(storageBoxManualMode,storageBoxArduinoComm, semaphoreArduinoComm);
        ServerHandler serverHandler = new ServerHandler(storageBoxSend,storageBoxReceive, semaphoreJSONdata, portNumber, storageBoxManualMode,semaphoreManualData); //storageBoxManualMode er overflødig
        GPSReader reader = new GPSReader(storageBoxGPS, storageBoxNorthEast, semaphoreGPScoordinates, semaphoreNorthEast);//Flytt til ServerHandler IF()
        PlatformMode platformMode = new PlatformMode(arduinoHandler, storageBoxPlatformMode, semaphorePlatformMode);
        LidarHandler lidarHandler = new LidarHandler(arduinoHandler.getArduino("LidarHandler"));
        Conductor conductor = new Conductor(storageBoxGPS,storageBoxNorthEast,storageBoxSend,storageBoxReceive, storageBoxPlatformMode ,
                                  semaphoreGPScoordinates,semaphoreNorthEast, semaphoreJSONdata,semaphoreWaypoint, semaphorePlatformMode);

// ServerSocketWorker serverSocketWorker = new ServerSocketWorker(storageBoxGPS, semaphoreGPScoordinates, portNumber);//test
        

        //Setter navn og Starter Tråder
        conductor.setName("Conductor Thread");
        conductor.start();
        
        Thread serverHandlerThread = new Thread(serverHandler);
        serverHandlerThread.setName("Server Handler Thread");
        serverHandlerThread.start();
        
        reader.setName("GPS-Reader Thread");
        reader.start();//Flytt til ServerHandler i IF(), likt som ServerSocketWorker
        platformMode.setName("Platform Mode Thread");
        platformMode.start();   
    }
}
