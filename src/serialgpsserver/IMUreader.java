/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serialgpsserver;

import org.json.JSONObject;

/**
 *
 * @author Matias
 */
public class IMUreader extends Thread {

    private double yaw;
    private double pitch;
    private double roll;
    
    private JSONObject jsonIMU;

    private boolean isFinished;

    public IMUreader() {
        //Json objekt til data fra IMU
        jsonIMU = new JSONObject();
        
        //Kanskje variablene kan droppes? eller må dette leses in en og en for så å putte i Json objekt?
        yaw = 0.0;
        pitch = 0.0;
        roll = 0.0;
        isFinished = false;
    }

    /**
     *
     */
    @Override
    public void run() {

    }

//Metoder for å hente ut data til videre overføring til ArduinoHandler    
    public synchronized double getYawValue() {
        return yaw;
    }

    public synchronized double getPitchValue() {
        return pitch;
    }

    public synchronized double getRollValue() {
        return roll;
    }
}
