package serialgpsserver;

/**
 * Pid kontroller for Autopilot og Dynamic Positioning
 * @author Albert
 */
public class PIDController {
//
    //IO Variables

    private double outputVariable;

    //Gains
    private double Kp;
    private double Ki;
    private double Kd;

    //Class variables
    private double integralTerm;
    private double lastError;
    private final double cycleTimeInSeconds;

    private double maxOutput;
    private double minOutput;

    public PIDController() {
        outputVariable = 0.0;
        integralTerm = 0.0;
        lastError = 0.0;

        maxOutput = 2 * 110.0;//Maks output jaging og tverrskipss
        minOutput = 2 * -110.0;//Minimum output jaging og tverrskips

        Kp = 1.0;
        Ki = 0.0;
        Kd = 0.0;
        cycleTimeInSeconds = 0.2;
    }

    /**
     * Beregner output for regulatoren
     *
     * @param newInput
     * @param referenceVariable
     * @param continuous
     * @return
     */
    public double computeOutput(double newInput, double referenceVariable,
            boolean continuous) {

        double error = referenceVariable - newInput;

        // Dersom kontinuerlig kan wrap around
        if (continuous) {
            maxOutput = 121.0; //torque(yaw)
            minOutput = -121.0; //torque(yaw)
            if (Math.abs(error) > 180) {
                if (error > 0) {
                    error = error - 360.0;
                } else {
                    error = error + 360.0;
                }
            }
        }
        // Integrator anti-windup og integrator ledd
        if ((integralTerm + error * cycleTimeInSeconds * Ki) < maxOutput
                && (integralTerm + error * cycleTimeInSeconds * Ki)
                > minOutput) {
            integralTerm += Ki * error * cycleTimeInSeconds;
        }
        double dError = (error - lastError) / cycleTimeInSeconds;
        //Beregn PID Output
        outputVariable = Kp * error + integralTerm + Kd * dError;
        limitOutputVariable();
        lastError = error;
        return outputVariable;

    }

    /**
     * resetter feil
     */
    public void resetErrors() {
        integralTerm = 0;
        lastError = 0;
    }

    /**
     * setter forsterkningskonstant for denne regulatoren
     *
     * @param Kp
     * @param Ki
     * @param Kd
     */
    public void setTunings(float Kp, float Ki, float Kd) {
        this.Kp = Kp;
        this.Ki = Ki;
        this.Kd = Kd;
    }

    /**
     * returener forsterkningskonstantene for denne regulatoren
     *
     * @return
     */
    public double[] getTunings() {
        return new double[]{Kp, Ki, Kd};
    }

    /**
     * setter forsterkningskonstantene for vdenne regulatoren
     *
     * @param gainChanged
     * @param newControllerGain
     */
    void setGain(int gainChanged, double newControllerGain) {
        switch (gainChanged) {
            case 1:
            case 4:
            case 7:
                Kp = newControllerGain;
                break;
            case 2:
            case 5:
            case 8:
                Ki = newControllerGain;
                break;
            case 3:
            case 6:
            case 9:
                Kd = newControllerGain;
                break;
        }
    }

    /**
     * begrenser output variablene
     */
    private void limitOutputVariable() {
        if (outputVariable > maxOutput) {
            outputVariable = maxOutput;
        } else if (outputVariable < minOutput) {
            outputVariable = minOutput;
        }
    }

}
