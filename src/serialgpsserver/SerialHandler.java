/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serialgpsserver;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author RobinBergset
 */
public class SerialHandler implements SerialPortEventListener {

    //Felt
    //private Enumeration portList;
    //private CommPortIdentifier portId;
   // private HashMap<String, CommPortIdentifier> comList;
    private String portName;
    private SerialPort serialPort;
    private CommPortIdentifier portId;
    private BufferedReader input;
    private OutputStream output;
    private boolean serialReady = false;
    private static final int TIME_OUT = 2000;
    private int DATA_RATE = 9600;
    private boolean isConnected = false;
    
    private String dataFromArduino = "Empty";
    private long time;
    private long testTime;

    /**
     * Initialiserer variabler
     */
    public SerialHandler(String serialPort, int baudRate, CommPortIdentifier portId) {
        
        this.portName = serialPort;
        this.DATA_RATE = baudRate;
        this.portId = portId;
        
        connect(portName);
        
        System.out.println("Arduino up and running");
  
    }

    /**
     *
     * @param port
     */
    public void connect(String port) {
        try {
            serialPort = (SerialPort) portId.open(this.getClass().getName(), TIME_OUT);
            serialPort.setSerialPortParams(DATA_RATE,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);
            input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
            output = serialPort.getOutputStream();
            serialReady = true;
            serialPort.addEventListener(this);
            serialPort.notifyOnDataAvailable(true);
            System.out.println("COM-port in use: " + serialPort.getName());
            isConnected = true;

        } catch (Exception e) {
            System.err.println(e.toString());
            System.out.println("Not connected");
            isConnected = false;
        }
    }

    @Override
    public void serialEvent(SerialPortEvent spe) {
        //System.out.println("SerialEVENT metode kjøres");
        if (spe.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
            try {
                boolean inputTest = input.ready();
                if(inputTest){
                String tempDataFromArduino = input.readLine(); 
                    //System.out.println(tempDataFromArduino);
                if(tempDataFromArduino.startsWith("{")){
                    dataFromArduino = tempDataFromArduino;
                    //System.out.println(dataFromArduino);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(SerialHandler.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("SerialPortEvent readLine error");
            }
        }      
    }

    public boolean send(JSONObject tempJSON){
       
        boolean isSent = false;
        time = System.currentTimeMillis();
        if(time > testTime){
            testTime = time + 1000;
        try {
        if (serialReady) { 
            String tempString = tempJSON.toString();
                byte[] valuesByte = tempString.getBytes(); 
                output.write(valuesByte);
                output.flush();
                isSent = true;
            }
        }
          catch (IOException ex) {
                Logger.getLogger(SerialHandler.class.getName()).log(Level.SEVERE, null, ex);
                isSent = false;
                System.out.println("isSent = " + isSent);
            }
        } 
        return isSent;
    }

    public BufferedReader getInputReader() {
        return input;
    }      
    
    public void close(){
        serialPort.close();
    }
    
        public String getArduinoData(){
        return dataFromArduino;
    }
        
        public boolean isConnected(){
            return isConnected;
        }
}
