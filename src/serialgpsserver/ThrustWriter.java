package serialgpsserver;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author root
 */
public class ThrustWriter {

    private int pulseWidth1;
    private int pulseWidth2;
    private int pulseWidth3;
    private int pulseWidth4;
    
    private int i = 0;

    private SerialHandler thrustArduino;
    private JSONObject thrustValuesToArduino;

    /**
     * Klasse for å konvertere newton til pwm og skrive verdier
     *
     * @param serialConnection
     * @param ID
     */
    public ThrustWriter(SerialHandler thrustArduino) {
        pulseWidth1 = 0;
        pulseWidth2 = 0;
        pulseWidth3 = 0;
        pulseWidth4 = 0;

        this.thrustArduino = thrustArduino;
        thrustValuesToArduino = new JSONObject();
    }

    /**
     * skriver verdier via SerialConnection
     */
    public void writeThrust() {
        try {           
            thrustValuesToArduino.put("smcSerial1", pulseWidth1);
            thrustValuesToArduino.put("smcSerial2", pulseWidth2);
            thrustValuesToArduino.put("smcSerial3", pulseWidth3);
            thrustValuesToArduino.put("smcSerial4", pulseWidth4);
            System.out.println(thrustValuesToArduino);
            thrustArduino.send(thrustValuesToArduino);

        } catch (JSONException ex) {
            Logger.getLogger(ThrustWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * setter pwm variablene
     *
     * @param newton
     */
    public void setThrustForAll(double[] newton) {
        pulseWidth1 = newtonToPulseWidth(newton[0]);
        pulseWidth2 = newtonToPulseWidth(newton[1]);
        pulseWidth3 = newtonToPulseWidth(newton[2]);
        pulseWidth4 = newtonToPulseWidth(newton[3]);
    }

    /**
     * konverterer newton til pwm
     *
     * @param xNewton
     * @return
     */
    public int newtonToPulseWidth(double xNewton) {
        

        int pulseWidth = 0;

        if (xNewton > 0.0) {
            double pulseWidthDouble = 32*xNewton;
            pulseWidth = (int) pulseWidthDouble;
        } else if (xNewton <= 0.0) {

            double pulseWidthDouble = 32*xNewton;
            pulseWidth = (int) pulseWidthDouble;
        } else {
            pulseWidth = 0;
        }

        if (pulseWidth < -3200) {
            pulseWidth = -3200;
        }
        if (pulseWidth > 3200) {
            pulseWidth = 3200;
        }
        return pulseWidth;
    }

    /**
     * lukker seriell forbindelsen
     */
    void closeSerialConn() {
        thrustArduino.close();
        System.out.println("ThrustWriter: Connection closed");
    }

}
