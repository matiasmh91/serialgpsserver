package serialgpsserver;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedReader;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Klasse for AutoPilot, "Følger waypoints"
 *
 * @author Matias
 */
public class AutoPilot extends Thread {

    public static final double RADIUS = 6378137.0;
    private static double distanceToWaypoint;
    private StorageBoxPlatformMode storageBoxAutoPilot;
    private Semaphore semaphoreAutoPilot;

    private JSONObject xyPosDataJson; //Felt for GPS rådata
    //private JSONObject rawWaypointData; //Felt for Waypoint rådata
    private JSONObject headingAndVectLength; // Felt for kurs og avstand
    private JSONObject jsonCourse;
    //private JSONObject arduinoData;

    //private BufferedReader input;

    private double headingAngleWaypoint;//Felt for vinkel til waypoint i grader
    private double platformHeading; // Vinkel på plattfrm ift sant nord
    private double headingFromArduino;

    private double outputX;
    private double outputY;
    private double outputN;

    private double refLatBody = 62.00000;
    private double refLonBody = 6.000000;

    private final ThrustAllocator thrustAllocator;
    private final ArduinoHandler ardHandler;
    private RotationMatrix Rz;
    private ThrustWriter tWriter;
    private double[] forceOutputNewton;

    private final PIDController xNorthPID;
    private final PIDController yEastPID;
    private final PIDController headingPID;
    private boolean isRunning;

    private long time;
    private long testTime;

    /**
     * Autopilot Konstruktør
     *
     * @param storageBoxAutoPilot Fellerobjekt Conductor og AutoPilot
     * @param semaphoreAutoPilot Fellerobjekt Conductor og AutoPilot
     * @param ardHandler Kontakten med Arduinoer
     */
    public AutoPilot(StorageBoxPlatformMode storageBoxAutoPilot, Semaphore semaphoreAutoPilot, ArduinoHandler ardHandler) {

        this.storageBoxAutoPilot = storageBoxAutoPilot;
        this.semaphoreAutoPilot = semaphoreAutoPilot;
        this.ardHandler = ardHandler;
        this.headingAndVectLength = new JSONObject();
        this.jsonCourse = new JSONObject();
        forceOutputNewton = new double[4];
        thrustAllocator = new ThrustAllocator();
        if (ardHandler.isRemoteArdConnected()) {
            tWriter = new ThrustWriter(ardHandler.getArduino("remoteOperation"));
        }
        xNorthPID = new PIDController();
        yEastPID = new PIDController();
        headingPID = new PIDController();
        //pid tunings
        xNorthPID.setTunings(30000, 1, 10);
        yEastPID.setTunings(30000, 1, 10);
        
    }

    /**
     * Method for å hente heading
     *
     * @return jsonobjekt med vinkel og lengde til ønsket waypoint
     */
    public JSONObject calculateHeadingAndDistance() {
        try {
            double rawLat = xyPosDataJson.getDouble("xyLatBody"); //Rådata fra GPS
            double rawLon = xyPosDataJson.getDouble("xyLonBody"); //Rådata fra GPS
            double wpLat = xyPosDataJson.getDouble("xyLatWaypoint"); //Waypointdata fra app
            double wpLon = xyPosDataJson.getDouble("xyLonWaypoint"); //Waypointdata fra app
            double GPSSpeed = xyPosDataJson.getDouble("speed");
//            if(GPSSpeed < 0.5){
//                
//                platformHeading = arduinoData.getDouble("Heading");
//            }
            double latitude1 = Math.toRadians(rawLat);
            double latitude2 = Math.toRadians(wpLat);
            double longDiff = Math.toRadians(wpLon - rawLon);
            double y = Math.sin(longDiff) * Math.cos(latitude2);
            double x = Math.cos(latitude1) * Math.sin(latitude2) - Math.sin(latitude1) * Math.cos(latitude2) * Math.cos(longDiff);
            double vectorLength = Math.abs((wpLat - (rawLat)) + (wpLon - (rawLon)));
            //Putter til jsonObjekt for senere håntering
            headingAngleWaypoint = (Math.toDegrees(Math.atan2(y, x)) + 360) % 360;
            headingAndVectLength.put("heading", headingAngleWaypoint);
            headingAndVectLength.put("length", vectorLength);

        } catch (JSONException ex) {
            Logger.getLogger(AutoPilot.class.getName()).log(Level.SEVERE, null, ex);
        }
        return headingAndVectLength;
    }

    @Override
    public void run() {

        //Boolean for å bryte while
        isRunning = true;
        if (ardHandler.isRemoteArdConnected()) {
            getHedingFromArduino();
        }
        try {
            while (isRunning) {
                time = System.currentTimeMillis();
                if (time > testTime) {
                    testTime = time + 500;
                    //Tar semaforen for storageBoxAutoPilot
                    semaphoreAutoPilot.acquire();
                    //Sjekker at storageBoxAutoPilot har fått koordinater fra gps/gui
                    boolean isAvailable = storageBoxAutoPilot.getAvailable();

                    if (isAvailable) {
                        //Lagrer jsonobjektene i egne json objekter
                        xyPosDataJson = new JSONObject();
                        xyPosDataJson = storageBoxAutoPilot.getGPSdata();
                        platformHeading = bearing();
                    }
                    //haversine(double latPlatform, double lonPlatform, double latWaypoint, double lonWaypoint)
                    if (xyPosDataJson.length() > 0 && xyPosDataJson != null && (haversine(xyPosDataJson.getDouble("lat"), xyPosDataJson.getDouble("lon"), xyPosDataJson.getDouble("waypointLat"), xyPosDataJson.getDouble("waypointLon"))) < 5) {
                        storageBoxAutoPilot.setHasArrivedToWaypoint(true);
                    } else {
                        storageBoxAutoPilot.setHasArrivedToWaypoint(false);
                    }
                    //Slipper semaforen så storageBoxAutoPilot kan entres av andre tråder.                
                    semaphoreAutoPilot.release();
                    //Sjekker om kriteriene for ny kurs er oppfyllt, json objektene må inneholde lat og lon  
                    if (xyPosDataJson.length() > 0) {
                        jsonCourse = calculateHeadingAndDistance();
                        System.out.println("Heading: " + jsonCourse.getString("heading") + " Degrees");
                        //int led = testHeading(); // Test program for å kjøre test med led og Arduino
                        //Henter output fra hver PID regulator
                        double X = xNorthPID.computeOutput((xyPosDataJson.getDouble("xyLatBody")*100), (xyPosDataJson.getDouble("xyLatWaypoint")*100), false);
                        double Y = yEastPID.computeOutput((xyPosDataJson.getDouble("xyLonBody")*100), (xyPosDataJson.getDouble("xyLonWaypoint")*100), false);
                        double N = headingPID.computeOutput(platformHeading, headingAngleWaypoint, true);
                        System.out.println("X output: " + X + " Y Output: " + Y +" N output: " + N);
                        //setter kreftene X og Y, og momentet N. 
                        setPIDOutputVector(X, Y, N);//synchronized   

                        Rz = new RotationMatrix(jsonCourse.getDouble("heading")); //HEADING MÅ VÆRE FRA IMU
                        double[] XYNtransformed = Rz.multiplyRzwithV(outputX, outputY, outputN);
                        forceOutputNewton = thrustAllocator.calculateOutput(XYNtransformed);

                        if (ardHandler.isRemoteArdConnected()) {
                            tWriter.setThrustForAll(forceOutputNewton);
                            tWriter.writeThrust();
                        } else {
                            System.out.println("Arduino not connected");
                        }
                    }
                }
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(AutoPilot.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(AutoPilot.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error XYNtransformed output force");
        }

    }

    private double getHedingFromArduino() {
        SerialHandler stabArdu = ardHandler.getArduino("StabilityArduino");
        if (ardHandler.isStabilityArdConnected()) {
            String temp = stabArdu.getArduinoData();
            headingFromArduino = Double.parseDouble(temp);
        }
        return headingFromArduino;
    }

    /**
     * Setter PID kreftene og momentet.
     *
     * @param X
     * @param Y
     * @param N
     */
    public synchronized void setPIDOutputVector(double X, double Y, double N) {
        outputX = X;
        outputY = Y;
        outputN = N;
    }

    public int testHeading() {
        double testHeading = bearing();
        System.out.print("GPS heading | ");
        System.out.println(testHeading);
        double diff = headingAngleWaypoint - testHeading;
        int led = 0;
        if (diff < 10 && diff > -10) {
            led = 1;
        } else if (diff > 10) {
            led = 2;
        } else if (diff < -10) {
            led = 3;
        }
        return led;
    }

    /**
     * Method for å hente heading
     *
     * @return jsonobjekt med vinkel og lengde til ønsket waypoint
     */
    public double calculateHeading() {
        double GPSHeading = 0;
        try {

            double currentLatBody = xyPosDataJson.getDouble("lat"); //Rådata fra GPS
            double currentLonBody = xyPosDataJson.getDouble("lon"); //Rådata fra GPS
            System.out.println(currentLatBody);
            System.out.println(refLatBody);
            System.out.println(currentLonBody);
            System.out.println(refLonBody);

            double headingRadians = Math.atan2(currentLatBody - refLatBody, currentLonBody - refLonBody);
            //double headingRadians = Math.atan2(refLatBody-currentLatBody, refLonBody-currentLonBody);
            GPSHeading = Math.toDegrees(headingRadians);
            GPSHeading = GPSHeading - 90;
            if (GPSHeading < 0) {
                GPSHeading = 360 + GPSHeading;
            }
            double vectorLength = Math.abs((refLatBody - (currentLatBody)) + (refLonBody - (currentLonBody)));
            //Putter til jsonObjekt for senere håntering

            refLatBody = xyPosDataJson.getDouble("lat"); //Rådata fra GPS
            refLonBody = xyPosDataJson.getDouble("lon"); //Rådata fra GPS

        } catch (JSONException ex) {
            Logger.getLogger(AutoPilot.class.getName()).log(Level.SEVERE, null, ex);
        }
        return GPSHeading;
    }

    protected double bearing() {
        try {
            double currentLatBody = xyPosDataJson.getDouble("lat"); //Rådata fra GPS
            double currentLonBody = xyPosDataJson.getDouble("lon"); //Rådata fra GPS

            double latitude1 = Math.toRadians(refLatBody);
            double latitude2 = Math.toRadians(currentLatBody);
            double longDiff = Math.toRadians(currentLonBody - refLonBody);
            double y = Math.sin(longDiff) * Math.cos(latitude2);
            double x = Math.cos(latitude1) * Math.sin(latitude2) - Math.sin(latitude1) * Math.cos(latitude2) * Math.cos(longDiff);

            refLatBody = xyPosDataJson.getDouble("lat"); //Rådata fra GPS
            refLonBody = xyPosDataJson.getDouble("lon"); //Rådata fra GPS

            return (Math.toDegrees(Math.atan2(y, x)) + 360) % 360;
        } catch (JSONException ex) {
            Logger.getLogger(AutoPilot.class.getName()).log(Level.SEVERE, null, ex);
            return 1;
        }
    }

    /**
     * Calculates the current position from the Platform to the destination
     * waypoint
     *
     * @param lat1 Platform position: latitude
     * @param lon1 Platform position: longitude
     * @param lat2 Waypoint from client: latitude
     * @param lon2 Waypoint from client: longitude
     * @return
     */
    public static double haversine(double lat1, double lon1, double lat2, double lon2) {
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        double a = Math.pow(Math.sin(dLat / 2), 2) + Math.pow(Math.sin(dLon / 2), 2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));
        distanceToWaypoint = RADIUS * c;
        return distanceToWaypoint;
    }

    public void close() {
        isRunning = false;
    }
}
