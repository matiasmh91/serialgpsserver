/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serialgpsserver;

import java.io.IOException;
import java.net.*;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Matias
 */
public class ServerHandler implements Runnable {
//Div felt. Husk å initialisere alle felter i konstruktor(god kodestil)    

    private StorageBoxSend storageBoxSend;
    private StorageBoxReceive storageBoxReceive;
    private StorageBoxManualmode sbmm;
    private Semaphore semaphoreJsonTCP;
    private Semaphore sem;
    private int portNumber;

    public ServerHandler(StorageBoxSend storageBoxS, StorageBoxReceive storageBoxR, Semaphore semaphoreJson, int portNumber, StorageBoxManualmode sbmm, Semaphore sem) {
        this.storageBoxSend = storageBoxS;
        this.storageBoxReceive = storageBoxR;
        this.semaphoreJsonTCP = semaphoreJson;
        this.portNumber = portNumber;
        this.sbmm = sbmm;
        this.sem = sem;

    }

    @Override
    public void run() {
        try {
            ServerSocket serverSock = new ServerSocket(7777); //Setter opp port på server

            while (true) {
                Socket socket = serverSock.accept();              //Godtar tilkopling til server
                System.out.println("Client connection established on port " + socket.getLocalPort()
                        + " from IP " + socket.getInetAddress().getHostAddress());
                ServerSocketWorkerSend serverWorkerSend = new ServerSocketWorkerSend(storageBoxSend, semaphoreJsonTCP, portNumber, socket);
                ServerSocketWorkerReceive serverWorkerReceive = new ServerSocketWorkerReceive(storageBoxReceive, semaphoreJsonTCP, portNumber, socket);

                Thread serverWorkerSendThread = new Thread(serverWorkerSend);
                serverWorkerSendThread.setName("Server Send Thread");
                serverWorkerSendThread.start();

                Thread serverWorkerReceiveThread = new Thread(serverWorkerReceive);
                serverWorkerReceiveThread.setName("Server Receive Thread");
                serverWorkerReceiveThread.start();

            }
        } catch (IOException ex) {
            Logger.getLogger(ServerHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
