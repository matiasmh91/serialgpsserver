/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serialgpsserver;

import org.json.JSONObject;

/**
 *
 * @author Matias
 */
public class StorageBoxArduinoComm {
    
    private boolean available = false; //Flag
    private JSONObject jsonArduinoData;

    /**
     * Boolean metode for å sjekke om koordinater er tilgjengelig
     *
     * @return true if coordinates are available
     */
    public boolean getAvailable() {
        return available;           //Returnerer flag
    }

    /**
     * Putter GPS koordinater i variabel som String
     *
     * @param coords Koorinater lagret som en string
     */
    public void putArduinoData(JSONObject jsonReceived) {
        if (available == false) {
            this.jsonArduinoData = jsonReceived; //Lagrer coordinater som JSONobject
        }
        available = true;                //Setter flag true
    }

    public JSONObject getArduinoData() {
        if (available == true) {
            available = false;
        }
        return this.jsonArduinoData;
    }
        
    
}
