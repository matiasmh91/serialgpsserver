/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serialgpsserver;

import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pmlei
 */
public class PlatformMode extends Thread {
 
    private ArduinoHandler ardHandler;
    private StorageBoxPlatformMode storageBoxPlatformMode;
    private Semaphore semaphorePlatformMode;
    private int currentMode = 0;
    
    private AutoPilot autoPilot;
    private ManualMode manualMode;
    
    public PlatformMode(ArduinoHandler ardHandler, StorageBoxPlatformMode storageBoxPlatformMode, Semaphore semaphorePlatformMode){
           
        this.ardHandler = ardHandler;
        this.storageBoxPlatformMode = storageBoxPlatformMode;
        this.semaphorePlatformMode = semaphorePlatformMode;
    }
    
    /**
     *
     */
    @Override
    public void run(){
         try {
             System.out.println("Ready to choose mode");
        boolean isRunning = true;
             startMode(currentMode);
        while(isRunning){       
                semaphorePlatformMode.acquire();
                boolean isAvailable = storageBoxPlatformMode.getAvailable();
                if(isAvailable){
                    int mode = storageBoxPlatformMode.getMode();
                    if(currentMode != mode){
                    startMode(mode);
                    currentMode = mode;
                    }
                }
            } 
            }catch (InterruptedException ex) {
                Logger.getLogger(PlatformMode.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void startMode(int mode){
        switch(mode){
            
            case 1:
                if(autoPilot != null && autoPilot.isAlive()) autoPilot.close();
                manualMode = new ManualMode(storageBoxPlatformMode, semaphorePlatformMode, ardHandler);
                manualMode.setName("Manual mode");
                manualMode.start();
                System.out.println("Starting Manual mode");
                break;
            case 2: 
                if(manualMode != null && manualMode.isAlive()) manualMode.close();
                autoPilot = new AutoPilot(storageBoxPlatformMode, semaphorePlatformMode, ardHandler);
                autoPilot.setName("Autopilot thread");
                autoPilot.start();
                System.out.println("Starting Autopilot");
                break;
            default:
            break;
        }
    }    
}
