/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serialgpsserver;

import java.util.Collection;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import no.ntnu.videostream.ImageStorageBox;
import org.json.JSONArray;

/**
 *
 * @author Matias
 */
public class Conductor extends Thread {

    private StorageBoxGPS storageBoxGPS;
    private StorageBoxNorthEastGPS storageBoxNorthEast;
    private StorageBoxSend storageBoxSend;
    private StorageBoxReceive storageBoxReceive;
    private StorageBoxPlatformMode storageBoxAutoPilot;

    private Semaphore semaphoreGPS;
    private Semaphore semaphoreNorthEast;
    private Semaphore semaphoreSend;
    private Semaphore semaphoreAutoPilot;
    private Semaphore semaphoreWaypoint;

    private boolean coordAvailable;
    private boolean northEastAvailable;
    private boolean hasDataFromClient;

    private JSONObject jsonToClient;
    private JSONObject jsonNorthEastCoord;
    private JSONObject jsonWaypoint;
    private List<Double> latCoordList;
    private List<Double> lonCoordList;
    private int lastCoordPos;

    private SerialHandler serialHandler;
    private double xyLatWaypoint;
    private double xyLonWaypoint;
    private double latRef;
    private double lonRef;
    private double spd;

    private NEDTransform gpsProc;
    private boolean hasWaypoint;
    private int waypointNumber;
    private boolean hasArrived;
    private JSONArray latList;
    private JSONArray lonList;

//Hold tunga beint i munnen
    Conductor(StorageBoxGPS storageBoxGPS, StorageBoxNorthEastGPS sbNorthEast, StorageBoxSend storageBoxSend, StorageBoxReceive storageBoxReceive,
            StorageBoxPlatformMode storageBoxAutoPilot, Semaphore semaphoreGPScoordinates, Semaphore semaNorthEast, Semaphore semaphoreJSONdata,
            Semaphore semaphoreWaypoint, Semaphore semaphoreAutoPilot) {
        this.storageBoxGPS = storageBoxGPS;
        this.storageBoxNorthEast = sbNorthEast;
        this.storageBoxSend = storageBoxSend;
        this.storageBoxReceive = storageBoxReceive;
        this.storageBoxAutoPilot = storageBoxAutoPilot;

        this.semaphoreSend = semaphoreJSONdata;
        this.semaphoreGPS = semaphoreGPScoordinates;
        this.semaphoreNorthEast = semaNorthEast;
        this.semaphoreAutoPilot = semaphoreAutoPilot;
        this.semaphoreWaypoint = semaphoreWaypoint;

        coordAvailable = false;
        northEastAvailable = false;
        jsonToClient = new JSONObject();
        jsonNorthEastCoord = new JSONObject();
        jsonWaypoint = new JSONObject();

        gpsProc = new NEDTransform();
        latCoordList = new ArrayList<>();
        lonCoordList = new ArrayList<>();
        hasWaypoint = false;
        waypointNumber = 0;
        hasArrived = false;
        
              
        //UDP STREAM
        int numberOfPermits = 1;
        boolean fairness = true;
        
        Semaphore semaphore = new Semaphore(numberOfPermits, fairness);
        ImageStorageBox imgStorageBox = new ImageStorageBox();

        VideostreamUDP imageCapture = new VideostreamUDP(semaphore, imgStorageBox);
        imageCapture.setName("Camera Frame Capture Thread");
        imageCapture.start();

    }

    /**
     * Override run() medtode i Thread klassen
     */
    @Override
    public void run() {

        boolean finished = false;

        try {
            while (!finished) {

                // Get GPS coordinate from GPS
                semaphoreGPS.acquire(); // Get flag from GPS storage box
                semaphoreNorthEast.acquire();

                northEastAvailable = storageBoxNorthEast.getAvailable();
                coordAvailable = storageBoxGPS.getAvailable();

                if (coordAvailable && northEastAvailable) {
                    storageBoxGPS.getGPSCoord(jsonToClient);
                    //System.out.println("At get statement: " + jsonToClient);
                    jsonNorthEastCoord = storageBoxNorthEast.getNorthEastJson();
                }

                // Release flag to GPS storage boxes
                semaphoreNorthEast.release();
                semaphoreGPS.release();

                //Innhenter semaphorer for videre håndtering av data
                semaphoreAutoPilot.acquire();
                semaphoreWaypoint.acquire();

                hasDataFromClient = storageBoxReceive.getAvailable();
                if (hasDataFromClient) {
                    jsonWaypoint = storageBoxReceive.getWaypointData();
                    System.out.println("Has data from client");
                }
                semaphoreWaypoint.release();
                if (jsonWaypoint.length() > 0 && jsonWaypoint.getInt("Mode") == 2) {

                    if (jsonWaypoint.length() > 0 && jsonWaypoint.getBoolean("NewWaypointList")) {
                        getWaypointLists();
                    }
                    // sends information to autopilot storage box
                    if (jsonWaypoint.length() > 0 && !jsonWaypoint.getBoolean("NewWaypointList") && !storageBoxAutoPilot.getAvailable()) {
                        posInfo();
                    }
                    if (hasWaypoint && storageBoxAutoPilot.hasArrivedToWaypoint()) {
                        setReferenceCoord();
                        posInfo();
                    }
                } else if (jsonWaypoint.length() > 0 && jsonWaypoint.getInt("Mode") == 1) {
                    semaphoreAutoPilot.acquire();
                    if(!storageBoxAutoPilot.getAvailable()){
                    storageBoxAutoPilot.putGPSWp(jsonWaypoint);
                    }
                    semaphoreAutoPilot.release();
                }
                //Sjekker om jsonToClient har elementer for så å aquire semafor til storage box,
                //putte json objektet der etter release semaforen
                if (jsonToClient.length() > 0) {
                    semaphoreSend.acquire();
                    storageBoxSend.putGPSCoord(jsonToClient);
                    // System.out.println("I send to client: " + jsonToClient);
                    semaphoreSend.release();
                }
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(Conductor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(Conductor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Setter referansekoordinater ved å multiplisere mottate waypoint
     * koordinater med PI/180
     */
    public void setReferenceCoord() {
        if (waypointNumber < lastCoordPos) {
            try {
                latRef = latList.getDouble(waypointNumber);
                lonRef = lonList.getDouble(waypointNumber);
                xyLatWaypoint = (latRef * Math.PI / 180.0);
                xyLonWaypoint = (lonRef * Math.PI / 180.0);
                System.out.println("Moving to waypoint " + (waypointNumber + 1) + " Latitude: " + latRef + " Longitude: " + lonRef);
                waypointNumber++;
            } catch (JSONException ex) {
                Logger.getLogger(Conductor.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            hasWaypoint = false;
        }
    }

    public void getWaypointLists() {
        hasWaypoint = true;
        waypointNumber = 0;
        try {
            latList = jsonWaypoint.getJSONArray("LatitudeList");
            lonList = jsonWaypoint.getJSONArray("LongitudeList");
            lastCoordPos = latList.length();
            System.out.println("Lengden på array: " + lastCoordPos);
            if (jsonWaypoint.getBoolean("NewWaypointList")) {
                setReferenceCoord();
                posInfo();
            }
            jsonWaypoint.put("NewWaypointList", false);
        } catch (JSONException ex) {
            Logger.getLogger(Conductor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void posInfo() {
        try {
            // sends information to autopilot storage box
            semaphoreAutoPilot.acquire();
            if (jsonWaypoint.length() > 0 && !storageBoxAutoPilot.getAvailable()) {
                jsonNorthEastCoord.put("xyLatWaypoint", xyLatWaypoint);
                jsonNorthEastCoord.put("xyLonWaypoint", xyLonWaypoint);
                jsonNorthEastCoord.put("waypointLat", latRef);
                jsonNorthEastCoord.put("waypointLon", lonRef);
                jsonNorthEastCoord.put("Mode", jsonWaypoint.get("Mode"));
                if (jsonNorthEastCoord.length() > 3) {
                    jsonNorthEastCoord = gpsProc.getFlatEarthCoordinates(jsonNorthEastCoord);
                    //System.out.println("NorthEast coords and speed: " + jsonNorthEastCoord);
                    storageBoxAutoPilot.putGPSWp(jsonNorthEastCoord);
                }
            } 
            semaphoreAutoPilot.release();
        } catch (InterruptedException ex) {
            Logger.getLogger(Conductor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(Conductor.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
    }
