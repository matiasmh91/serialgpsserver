/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serialgpsserver;

import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import no.ntnu.videostream.ImageStorageBox;
import no.ntnu.videostream.UDPCameraStream;
import org.json.JSONObject;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;

/**
 *
 * @author Matias
 */
public class VideostreamUDP extends Thread {
    
    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }
    
private boolean isManualMode;
    private Semaphore semaphoreVideoStream;
    private ImageStorageBox imageStorageBox;
    
    public VideostreamUDP(Semaphore semaphore, ImageStorageBox imgStorageBox) {
     this.semaphoreVideoStream = semaphore;
     this.imageStorageBox = imgStorageBox;
       // String ipAddress = "127.0.0.1";
        String ipAddress = "192.168.0.100";
        int portNumber = 5555;
     UDPCameraStream videoStream = new UDPCameraStream(semaphore, imgStorageBox, ipAddress, portNumber);
     videoStream.setName("UDP Camera Stream Thread");
        videoStream.start();
    }
    
    @Override
    public void run(){
       Mat webcamMatImage = new Mat();
       VideoCapture capture = new VideoCapture(0);
        int i = 0;
        isManualMode = true;

        while (isManualMode) {
            i++;
            try {
                capture.read(webcamMatImage);
                semaphoreVideoStream.acquire();
                if (!webcamMatImage.empty()) {
                    imageStorageBox.putImage(webcamMatImage);
                }
                semaphoreVideoStream.release();
            } catch (InterruptedException ex) {
                Logger.getLogger(VideostreamUDP.class.getName()).log(Level.SEVERE, null, ex);
            }
            //  System.out.println("Count = " + i );

            }
    }
    
}
