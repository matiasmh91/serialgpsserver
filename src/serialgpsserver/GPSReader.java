package serialgpsserver;

import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.Timer;
import java.util.TooManyListenersException;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @Edits of several functions: Matias
 */
public class GPSReader extends Thread implements SerialPortEventListener {

    //"DEVICE","path":"/dev/ttyACM0"   GPS tilkobling fra terminal
    private GPSData currentPosition;

    private long portScanTime = 2000;
    private long updateTime = 500;
    private boolean found = false;
    private InputStream inputStream;
    private GPSDataEventListener dataListener;
    private SerialPort gpsPort = null;
    private boolean running = true;
//Kode lagt til:
    private StorageBoxGPS storageBoxGPS;
    private StorageBoxNorthEastGPS storageBoxNorthEast;
    private Semaphore semaphoreGPS;
    private Semaphore semaphoreNorthEast;
    private boolean available;
    private double xyLatBody;
    private double xyLonBody;
    private double latRef;
    private double lonRef;
    private boolean northEastAvailable;
       
    private long gpsUpdateTime = 600;

    public GPSReader(StorageBoxGPS storageBoxGPS, StorageBoxNorthEastGPS storageBoxNorthEastGPS, Semaphore semaphoreGPS, Semaphore semaphoreNorthEast) {
        this.storageBoxGPS = storageBoxGPS;
        this.storageBoxNorthEast = storageBoxNorthEastGPS;
        this.semaphoreGPS = semaphoreGPS;
        this.semaphoreNorthEast = semaphoreNorthEast;
        this.available = true;
        this.northEastAvailable = true;
        
        
    }

    private static final String regExp = "((\\$GPGGA)|(\\$GPRMC)|(\\$GPGSA)|(\\$GPGLL)|(\\$GPGSV)|(\\$GPVTG)).*";

    //Må gå gjennom metoden under: findPort()
    /**
     *
     * @return
     */
    public boolean findPort() {
        // For linux(ubuntu)
        //System.setProperty("gnu.io.rxtx.SerialPorts", "/dev/ttyACM0");
System.out.println("GPS starts");
        Enumeration<CommPortIdentifier> enumer = CommPortIdentifier.getPortIdentifiers();

        while (enumer.hasMoreElements()) {
            CommPortIdentifier port = enumer.nextElement();

            if (port.getPortType() == CommPortIdentifier.PORT_SERIAL && !port.isCurrentlyOwned()) {
                SerialPort serialPort = null;
                try {
                    serialPort = (SerialPort) port.open("MY_PORT_NAME", 2000);

                    inputStream = null;
                    inputStream = serialPort.getInputStream();
                    if (inputStream == null) {
                        System.out.println("no input stream");
                        return false;

                    }
                    serialPort.addEventListener(this);

                    serialPort.notifyOnDataAvailable(true);

                    serialPort.setSerialPortParams(4800, SerialPort.DATABITS_8,
                            SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

                    Thread.sleep(getPortScanTime());
                } catch (PortInUseException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (TooManyListenersException e) {
                    e.printStackTrace();
                } catch (UnsupportedCommOperationException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (this.isFound()) {
                    System.out.println("Found Stream on com port " + serialPort.getName());
                    System.out.println("Waiting for client connection...");
                    serialPort.removeEventListener();
                    setDataListener(new GPSDataEventListener(inputStream));
                    this.gpsPort = serialPort;
                    try {
                        serialPort.addEventListener(getDataListener());
                        serialPort.notifyOnDataAvailable(true);
                    } catch (TooManyListenersException e) {
                        e.printStackTrace();
                    }
                    return true;
                } else {
                    try {
                        if (inputStream != null) {
                            inputStream.close();
                        }
                        if (serialPort != null) {
                            serialPort.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
       // System.out.println("No Stream found!");
        return false;
    }

    @Override
    public void run() {
        try {
            findPort();
            if(getDataListener() != null && getDataListener().isConnection()){
            setRunning(true);
            }
            while (isRunning()) {
                if (isFound() && getDataListener() != null && getDataListener().isConnection()) {
                    GPSData data = getDataListener().getLocationData();
                    setCurrentPosition(data);
                    

//Prøver å aquire semaphore                        
                    semaphoreGPS.acquire();
                    semaphoreNorthEast.acquire();
// Tenker å putte GPS koordinater som string i StorageBoxGPS objektets variabel
                    available = storageBoxGPS.getAvailable();
                    northEastAvailable = storageBoxNorthEast.getAvailable();
                    if (!available && !northEastAvailable && data != null && data.isConnection()) {
                        double lat = data.getLatitude();
                        double lon = data.getLongitude();
                        double spd = data.getSpeed();
                        storageBoxGPS.putGPSCoord(lat, lon, spd);
                        xyLatBody = (lat * (Math.PI) / 180.0);
                        xyLonBody = (lon * (Math.PI) / 180.0);
                        //System.out.println("Latitude: "+ lat + " Longditude: " + lon);
                        JSONObject xyNorthEast = new JSONObject();
                        xyNorthEast.put("xyLatBody", xyLatBody);
                        xyNorthEast.put("xyLonBody", xyLonBody);
                        xyNorthEast.put("speed", spd);
                        xyNorthEast.put("lat", lat);
                        xyNorthEast.put("lon", lon);
                        storageBoxNorthEast.putGPSCoord(xyNorthEast);

                    } else if (data != null && !data.isConnection()) {
                        System.out.println("No connection with GPS Satellites");
                    }

// Slipper semaphore                    
                    semaphoreGPS.release(); //Bruke finally block for å sikre release selv om exception?
                    semaphoreNorthEast.release();
//Print til terminal
                    // 
                } else {
                    if (getDataListener() != null) {
                        setDataListener(null);
                        gpsPort.close();
                        gpsPort.removeEventListener();
                        gpsPort = null;
                        setFound(false);
                        System.out.println("Not found");
                    }

             

                }
                Thread.sleep(getUpdateTime());
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    @Override
    public void serialEvent(SerialPortEvent event
    ) {

        if (event.getEventType() == SerialPortEvent.DATA_AVAILABLE && !found) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(getInputStream()));
            String line = null;
            try {
                if (reader.ready()) {
                    line = reader.readLine();
                    if (line != null && line.matches(regExp)) {
                        this.setFound(true);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public long getGPSshortUpdateTime(){
    return gpsUpdateTime;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    private void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    private void setFound(boolean found) {
        this.found = found;
    }

    public boolean isFound() {
        return found;
    }

    synchronized private void setCurrentPosition(GPSData currentPosition) {
        this.currentPosition = currentPosition;
    }

    synchronized public GPSData getCurrentPosition() {
        return currentPosition;
    }

    public void setPortScanTime(long portScanTime) {
        this.portScanTime = portScanTime;
    }

    public long getPortScanTime() {
        return portScanTime;
    }

    private void setDataListener(GPSDataEventListener dataListener) {
        this.dataListener = dataListener;
    }

    private GPSDataEventListener getDataListener() {
        return dataListener;
    }

    public void setRunning(boolean run) {
        this.running = run;
    }

    public boolean isRunning() {
        return running;
    }

    public boolean isConnection() {
        return isFound() && getCurrentPosition() != null && getCurrentPosition().isConnection();
    }

}
