package serialgpsserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Bakken
 */
public class ManualDataHandler extends Thread {

    StorageBoxManualmode sbmm = null;
    JSONObject jsonObject = null;
    private Socket socket = null;
    private Semaphore semaphore;
    Boolean available;

    public ManualDataHandler(Socket socket, StorageBoxManualmode storageBoxmm, Semaphore semaphore) {
        super("ManualDataHandler");
        this.socket = socket;
        this.sbmm = storageBoxmm;
        this.semaphore = semaphore;

    }

    ManualDataHandler(Socket socket, StorageBoxManualmode sbmm, Socket socket0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void run() {
        System.out.println("Hi, this is debugger");
        boolean finished = false; //Boolean for while loop i run() så den kan avsluttes uten "break;"

        try {
            BufferedReader bufRead = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
            while (!finished) {

                semaphore.acquire();
                available = sbmm.getAvailable();
                String str = bufRead.readLine();
                this.jsonObject = new JSONObject(str);
                System.out.println("ManualDataHandler got: " + jsonObject.get("Direction") + " from Android");
                System.out.println("ManualDataHandler got: " + jsonObject.get("Speed").toString() + " from Android");
                
                if(!available){
                    
                  sbmm.putManualData(jsonObject);
                System.out.println("ManualDataHandler put " + jsonObject.get("Direction") + " in the storagebox");
                System.out.println("ManualDataHandler put " + jsonObject.get("Speed") + " in the storagebox");
                    
                }
                semaphore.release();

            }

            socket.close();
        } /*catch (SocketException se) {
         System.out.println("Client closed connection to server. Ending client communication..");
         } catch (IOException e) {
         e.printStackTrace();
         }*/ catch (JSONException ex) {
            Logger.getLogger(ManualDataHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ManualDataHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(ManualDataHandler.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }

}
