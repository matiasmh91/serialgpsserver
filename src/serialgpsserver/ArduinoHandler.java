/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serialgpsserver;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Bakken, Per Martin, Matias
 */
public class ArduinoHandler {

    private Enumeration portList;
    private CommPortIdentifier portId;
    private HashMap<String, CommPortIdentifier> comList;

    private ArrayList<String> listOfPorts = new ArrayList<String>();
    
    private SerialHandler StabilityArduino;
    private SerialHandler remoteOperation;

    SerialPort serialPort;
    StorageBoxManualmode storageBoxManualMode;
    StorageBoxArduinoComm storageBoxArduinoComm;
    private byte[] b = new byte[2];
    private String[] d;

    String[] sa = new String[2];

    private JSONObject jsonRemoteControl;
    
    private Semaphore semaphore;
    Boolean available;

    private static final String PORT_NAMES[] = {
        "/dev/ttyUSB0",
        "COM4",};

    private BufferedReader input;
    private OutputStream output;
    //private DataOutputStream output;
    private static final int TIME_OUT = 2000;
    private static final int DATA_RATE = 9600;
    private SerialHandler LidarArduino;
    
    private String stabilityArd = "COM4";
    private boolean stabilityArdConnected = false;
    private String remoteArd = "COM6";
    private boolean remoteArdConnected = false;
    
    

    public ArduinoHandler(StorageBoxManualmode sbm, StorageBoxArduinoComm sbac, Semaphore semaphore) {
        this.storageBoxManualMode = sbm;
        this.storageBoxArduinoComm = sbac;
        this.semaphore = semaphore;

        // Json Objekt
        jsonRemoteControl = new JSONObject();

        comList = new HashMap<>();
        portList = CommPortIdentifier.getPortIdentifiers();
        while (portList.hasMoreElements()) {
            portId = (CommPortIdentifier) portList.nextElement();
            listOfPorts.add(portId.getName());
            comList.put(portId.getName(), portId);

            System.out.println(portId.getName());

        }
        connectToArduino();
    }
    
    public synchronized void close() {
        if (serialPort != null) {
            serialPort.removeEventListener();
            serialPort.close();
        }
    }

//    public synchronized void serialEvent(SerialPortEvent oEvent) {
//        if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
//            try {
//
//                byte[] inputLine = input.readLine().getBytes();
//                //dataFromArduino = new String(inputLine);
//                //System.out.println("Recived from the Arduino: " + inputLine + "     " + dataFromArduino);
//
//            } catch (Exception e) {
//                System.err.println(e.toString());
//            }
//        }
//    }

    public void sendData() {
        try {
            output = serialPort.getOutputStream();
            String[] sendValues = getData();
            String s1 = sendValues[0];
            String s2 = sendValues[1];
            String s3 = sendValues[0] + "," + sendValues[1];
            System.out.println(s3);
            output.write(s3.getBytes());
            output.flush();

        } catch (IOException ex) {
            Logger.getLogger(ArduinoHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void readData() {

        try {
            String i;
            BufferedReader bf = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
            i = bf.readLine();
            System.out.println("Recived from arduino: " + i);
        } catch (IOException ex) {
            Logger.getLogger(ArduinoHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public String[] getData() {

        try {
            semaphore.acquire();
            available = storageBoxManualMode.getAvailable();

            if (available) {
                jsonRemoteControl = storageBoxManualMode.getManualData();

                System.out.println("ArduinoHandler got: " + jsonRemoteControl.get("Direction") + " and " + jsonRemoteControl.get("Speed") + " from the Storagebox");
            }
            sa[0] = jsonRemoteControl.get("Direction").toString();
            sa[1] = jsonRemoteControl.get("Speed").toString();

            semaphore.release();
        } catch (InterruptedException ex) {
            Logger.getLogger(ArduinoHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(ArduinoHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

        return sa;

    }

    private void connectToArduino() {

        for (String temp : listOfPorts) {
            if (temp.equalsIgnoreCase(stabilityArd)) {
                CommPortIdentifier portId = comList.get(stabilityArd);
                StabilityArduino = new SerialHandler(stabilityArd, 9600, portId);
                System.out.println("Connected to stability controll Arduino");
                stabilityArdConnected = true;
            }
            if(temp.equalsIgnoreCase(remoteArd)){
                CommPortIdentifier portId = comList.get(remoteArd);
                remoteOperation = new SerialHandler(remoteArd, 9600, portId);
                System.out.println("Connected to thruster controll Arduino");
                remoteArdConnected = true;
            }
            if(temp.equalsIgnoreCase("COM")){
                CommPortIdentifier portId = comList.get("COM");
                StabilityArduino = new SerialHandler("COM", 9600, portId);
            }
        }

    }

    public SerialHandler getArduino(String name){
        SerialHandler temp = null;
        if(name.equalsIgnoreCase("StabilityArduino")){
            temp = StabilityArduino;
        }
        else if(name.equalsIgnoreCase("remoteOperation")){
            temp =  remoteOperation;
        }
        else if(name.equalsIgnoreCase("LidarArduino")){
        temp = LidarArduino;
    }
        return temp;
    }
    
    public boolean isStabilityArdConnected(){
        return stabilityArdConnected;
    }
    
    public boolean isRemoteArdConnected(){
        return remoteArdConnected;
    }
           
}

