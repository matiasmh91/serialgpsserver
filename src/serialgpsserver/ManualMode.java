/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serialgpsserver;

import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import no.ntnu.videostream.ImageStorageBox;
import no.ntnu.videostream.UDPCameraStream;
import org.json.JSONObject;

/**
 * This class provide manual control of the craft from a tablet
 * @author pmlei
 */
public class ManualMode extends Thread{
    
    ArduinoHandler ardHandler;
    private boolean isRunning;
    private StorageBoxPlatformMode storageBoxManualMode;
    private Semaphore semaphoreManualMode;
    private JSONObject manualModeJson;
    
    /**
     * Manual mode constructor
     * @param storageBoxManualMode
     * @param semaphoreManualMode
     * @param ardHandler
     */
    public ManualMode(StorageBoxPlatformMode storageBoxManualMode, Semaphore semaphoreManualMode, ArduinoHandler ardHandler){

        this.ardHandler = ardHandler;
        this.storageBoxManualMode = storageBoxManualMode;
        this.semaphoreManualMode = semaphoreManualMode;
    }
    
@Override
    public void run() {
 
        //Boolean for å bryte while
        isRunning = true;
   
            while (isRunning) {
            try {
                //Tar semaforen for storageBoxAutoPilot
                semaphoreManualMode.acquire();
                //Sjekker at storageBoxAutoPilot har fått koordinater fra gps/gui
                boolean isAvailable = storageBoxManualMode.getAvailable();
              
                if (isAvailable) {
                    //Lagrer jsonobjektene i egne json objekter
                    
                    manualModeJson = new JSONObject();
                    manualModeJson = storageBoxManualMode.getGPSdata();
                }
                semaphoreManualMode.release();
                
                if(ardHandler.isRemoteArdConnected()){
                    ardHandler.getArduino("remoteOperation").send(manualModeJson);
                    System.out.println("Data in manual mode: " +manualModeJson);
                    
                }
                else System.out.println("Platform contril arduino not connected");
                if(ardHandler.isStabilityArdConnected()){
                    String dataFromStabilityArduino = ardHandler.getArduino("StabilityArduino").getArduinoData();
                    semaphoreManualMode.tryAcquire();
                //Sjekker at storageBoxAutoPilot har fått koordinater fra gps/gui
                isAvailable = storageBoxManualMode.getAvailable();
                if (isAvailable) {                    
                    storageBoxManualMode.putData("ArduinoHeading", dataFromStabilityArduino);
                }
                semaphoreManualMode.release();
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(ManualMode.class.getName()).log(Level.SEVERE, null, ex);
            }
                }
            }

    public void close() {
        isRunning = false;
    }
}
