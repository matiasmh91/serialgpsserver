/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serialgpsserver;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
 import org.json.JSONObject;

/**
 *
 * @author Matias
 */
public class StorageBoxPlatformMode {
//Felt

    private boolean available = false; //Flag
//    private double lat;
//    private double lon;
//    private double speed;
    private int mode = 0;
    private JSONObject jsonWPData;
    private boolean hasArrived;

    /**
     * Boolean metode for å sjekke om ny koordinat er tilgjengelig
     *
     * @return true if coordinates are available
     */
    public synchronized boolean getAvailable() {
        return available;           //Returnerer flag
    }
    /**
     * Putter waypoint data fra GUI
     *
     * @param jsonData
     */
    public void putGPSWp(JSONObject jsonData) {
        if (available == false) {
            this.jsonWPData = jsonData; //Lagrer coordinater som JSONobject
        }
        available = true;                //Setter flag true
    }
    
    /**
     * Henter waypoint koordinater satt i GUI
     * @return
     */
    public int getMode() {
        try {
            if(jsonWPData.length() > 0) mode = jsonWPData.getInt("Mode");
            else System.out.println("No mode recived");
        } catch (JSONException ex) {
            Logger.getLogger(StorageBoxPlatformMode.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mode;
    }
    
    //***********************************************************
    //**************Test Metoder ********************************
    //***********************************************************
        /**
     * Putter waypoint data fra GUI
     *
     * @param jsonData
     */
    public void putGPSCoord(JSONObject jsonData) {
        if (available == false) {
            this.jsonWPData = jsonData; //Lagrer coordinater som JSONobject
        }
        available = true;                //Setter flag true
    }
    
    /**
     * Henter waypoint koordinater satt i GUI
     * @return
     */
    public JSONObject getGPSdata(){
        if (available == true) {
            available = false;
        }
        //System.out.println("Data in PM storage box: " +jsonWPData);
        return this.jsonWPData;

    }
 /**
     * Return true if the platform has arrived the waypoint received from client.
     * @return hasArrived
     */
    public boolean hasArrivedToWaypoint(){   
    return hasArrived;
    }
    
    public void setHasArrivedToWaypoint(boolean value){
        this.hasArrived = value;
    } 
    
    public void putData(String key, String data){
         if (available == false) {
             try {
                 this.jsonWPData.put(key, data);
             } catch (JSONException ex) {
                 Logger.getLogger(StorageBoxPlatformMode.class.getName()).log(Level.SEVERE, null, ex);
             }
        }
        available = true;  
    }
}
