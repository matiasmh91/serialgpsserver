/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serialgpsserver;

import java.net.*;
import java.io.*;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.*;

/**
 *
 * @author Matias
 */
public class ServerSocketWorkerSend implements Runnable {

    //Felt
    private Socket socket = null;
    private StorageBoxSend storageBoxSend;
    private Semaphore semaphore;
    private int portNumber;
    private boolean available;
    private  boolean finished;
    
    public ServerSocketWorkerSend(StorageBoxSend storageBox, Semaphore semaphore, int portNumber, Socket socket) {
        //super("ServerSocketWorker");
        this.socket = socket;
        this.storageBoxSend = storageBox;
        this.semaphore = semaphore;
        this.portNumber = portNumber;
        this.available = false;

    }

    @Override
    public void run() {
        finished = false; //Boolean for while loop i run() så den kan avsluttes uten "break;"

        try {
            PrintWriter outStreamWrite = new PrintWriter(this.socket.getOutputStream(), true);
            double p = 62.467067;
            while (!finished) {
                JSONObject jsonData = new JSONObject();
                semaphore.acquire();

                available = storageBoxSend.getAvailable();

                if (available) {
                    jsonData = storageBoxSend.getGPSdata();
                    outStreamWrite.println(jsonData);
                    //System.out.println("JSONData_Sent_To_Client: " + jsonData);
                }
                semaphore.release();
                jsonData.put("message", "Good morning");
                //jsonData.put("lat", p);
                //jsonData.put("lon", 6.238072);
                //outStreamWrite.println(jsonData);
                p += 0.000000001;
                //System.out.println(p);
                if(!socket.getInetAddress().isReachable(1000)){
                    finished = true;
                }
            }

            socket.close();
        } catch (SocketException se) {
            System.out.println("Client closed connection to server. Ending client communication.." + se);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException ex) {
            Logger.getLogger(ServerSocketWorkerSend.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (JSONException ex) {
            Logger.getLogger(ServerSocketWorkerSend.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
