/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serialgpsserver;

import org.json.JSONObject;

/**
 *
 * @author Matias
 */
public class StorageBoxSend {

    private boolean available = false; //Flag
    private JSONObject jsonData;        // Variabel for å lagre koordinater

    /**
     * Boolean metode for å sjekke om koordinater er tilgjengelig
     *
     * @return true if coordinates are available
     */
    public boolean getAvailable() {
        return available;           //Returnerer flag
    }

    /**
     * Putter GPS koordinater i variabel som String
     *
     * @param coords Koorinater lagret som en string
     */
    public void putGPSCoord(JSONObject jsonData) {
        if (available == false) {
            this.jsonData = jsonData; //Lagrer coordinater som JSONobject
        }
        available = true;                //Setter flag true
    }

    public JSONObject getGPSdata() {
        if (available == true) {
            available = false;
        }
        return this.jsonData;
    }

//    /**
//     * Henter ut Koordinater som string
//     * @return
//     */
//    public String getGPSCoord() {
//        if (available == true) { //Sjekker om Available = true
//            available = false;   // setter available = false før koden fortsetter.
//        }
//        return coordinates;      //Returnerer Koordinat info som en string
//    }
}
