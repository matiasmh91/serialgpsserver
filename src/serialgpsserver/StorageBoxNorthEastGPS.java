/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serialgpsserver;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Matias
 */
public class StorageBoxNorthEastGPS {
//Felt

    private boolean available = false; //Flag
    private double lat;
    private double lon;
    private double speed;
    
    private JSONObject northEastJson;

    /**
     * Boolean metode for å sjekke om koordinater er tilgjengelig
     *
     * @return true if coordinates are available
     */
    public boolean getAvailable() {
        return available;           //Returnerer flag
    }

    /**
     * Putter GPS koordinater i variabel som JSONObjekt Putter feltene; lat,
     * lon, speed, henter disse ut som JSONobjekt
     *
     * @param jsonObj "leash" fra Conductor klassen
     */
    public void getGPSCoord(JSONObject jsonObj) {
        if (available == true) {
            try {
                //Lagrer coordinater og speed som double
                jsonObj.put("latNE", lat);
                jsonObj.put("lonNE", lon);
                jsonObj.put("speedNE", speed);
            } catch (JSONException ex) {
                Logger.getLogger(StorageBoxGPS.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        available = false;                //Setter flag true
    }

    /**
     * Putter datafelter fra GPSRead klassen
     *
     * @param latitude, double
     * @param longitude, double
     * @param speed, double
     */
    public void putGPSCoord(Double latitude, Double longitude, Double speed) {
        if (available == true) { //Sjekker om Available = true
            available = false;   // setter available = false før koden fortsetter.
        }
        this.lat = latitude;
        this.lon = longitude;
        this.speed = speed;

        available = true;
    }

    public void putGPSCoord(JSONObject xyNorthEast) {
        if(available == false){
            this.northEastJson = xyNorthEast;
        }
        available = true;
    }

    public JSONObject getNorthEastJson(){
        if(available == true){
            available = false;
        }
        return this.northEastJson;
    }
}
