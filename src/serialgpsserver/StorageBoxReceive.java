/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serialgpsserver;

import org.json.JSONObject;

/**
 *
 * @author Matias
 */
class StorageBoxReceive {

    private boolean available = false; //Flag
    private String coordinates;        // Variabel for å lagre koordinater
    private JSONObject jsonWaypointData;

    /**
     * Boolean metode for å sjekke om koordinater er tilgjengelig
     *
     * @return true if coordinates are available
     */
    public boolean getAvailable() {
        return available;           //Returnerer flag
    }

    /**
     * Putter GPS koordinater i variabel som String
     *
     * @param coords Koorinater lagret som en string
     */
    public void putWaypointCoord(JSONObject jsonReceived) {
        if (available == false) {
            this.jsonWaypointData = jsonReceived; //Lagrer coordinater som JSONobject
        }
        available = true;                //Setter flag true
    }

    public JSONObject getWaypointData() {
        if (available == true) {
            available = false;
        }
        return this.jsonWaypointData;
    }

    public void getWaypointData(JSONObject jsonWaypoint) {
        if (available == true) {
            available = false;//Setter flag true
        jsonWaypoint.equals(this.jsonWaypointData);
        }
      //  this.jsonWaypointData = jsonWaypoint;
        
    }
}
