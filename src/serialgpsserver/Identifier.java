package serialgpsserver;

/**
 *
 * @author Albert
 */
public enum Identifier {
    GPS,IMU,THRUSTERS,WIND,SURGE,SWAY,HEADING;
    
}
