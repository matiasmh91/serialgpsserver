/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serialgpsserver;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.*;

/**
 *
 * @author Matias
 */
public class ServerSocketWorkerReceive implements Runnable {
//Felt

    private Socket socket = null;
    private StorageBoxReceive storageBoxReceive;
    private Semaphore semaphore;
    private int portNumber;
    private boolean available;
    private JSONObject jsonWaypointRecieved;
    private boolean finished;
    /**
     * Constructor
     * @param storageBox
     * @param semaphore
     * @param portNumber
     * @param socket
     */
    public ServerSocketWorkerReceive(StorageBoxReceive storageBox, Semaphore semaphore, int portNumber, Socket socket) {
        //super("ServerSocketWorker");  // Hva skjer her?
        
        this.socket = socket;
        this.storageBoxReceive = storageBox;
        this.semaphore = semaphore;
        this.portNumber = portNumber;
        this.available = false;
        
       }

    /**
     * Run metode, overrider Thread klassens run()
     */
    @Override
    public void run() {
        finished = false;
        try {
            while (!finished) {
                InputStreamReader inRead = new InputStreamReader(this.socket.getInputStream()); //leser input stream på socket
                BufferedReader bufRead = new BufferedReader(inRead); //buffer på in stream
               String jsonCoordMsg = bufRead.readLine();
                
                if(!socket.getInetAddress().isReachable(10000)){
                    finished = true;
                    bufRead.close();
                    inRead.close();
                    socket.close();
                    
                }
                
               if(jsonCoordMsg != null && jsonCoordMsg.startsWith("{")){
                   this.jsonWaypointRecieved = new JSONObject(jsonCoordMsg);
                  System.out.println("Recived from client: " +jsonCoordMsg);
               }
                if(jsonWaypointRecieved != null &&jsonWaypointRecieved.length()> 0){
                storageBoxReceive.putWaypointCoord(jsonWaypointRecieved);
//                System.out.println("Latitude: " + jsonWaypointRecieved.get("lat"));
//                System.out.println("Longditude: " + jsonWaypointRecieved.get("lon"));
                //System.out.println(msg); // Printer incoming on socket til terminal
                }
                
                if (jsonCoordMsg != null) {
                    PrintStream printStream = new PrintStream(socket.getOutputStream()); //Skriver til output mot klient at msg er mottat
                    printStream.println("Message recieved @ server");
       
                        

                    if (jsonCoordMsg.equals("bye")) {
                        System.out.println("Shutting down server...");
                        finished = true;
                    }

                }
                

            } // Avslutter Server(test)
        } catch (IOException ex) {
            Logger.getLogger(ServerSocketWorkerReceive.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(ServerSocketWorkerReceive.class.getName()).log(Level.SEVERE, null, ex);
        } 

    }
}
