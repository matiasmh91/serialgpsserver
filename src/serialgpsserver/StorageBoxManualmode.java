/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serialgpsserver;

import org.json.JSONObject;

/**
 *
 * @author Bakken
 */
public class StorageBoxManualmode {
    
    private boolean available = false; //Flag
   
    private JSONObject jsonManualData; // Object to store Manual data

    /**
     * Boolean method to check for new incoming data 
     *
     * @return true if data are available
     */
    public boolean getAvailable() {
        return available;           //Returnerer flag
    }

    /**
     * Method to put data i JSONObject
     */
    public void putManualData(JSONObject jsonReceived) {
        if (available == false) {
            this.jsonManualData = jsonReceived; //Lagrer data som JSONobject
        }
        available = true;                //Setter flag true
    }

    public JSONObject getManualData() {
        if (available == true) {
            available = false;
        }
        return this.jsonManualData;
    }
    
}
